Source: fortran-language-server
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Denis Danilov <danilovdenis@yandex.ru>
Section: editors
Priority: optional
Build-Depends: debhelper-compat (=13),
 dh-python,
 help2man,
 pybuild-plugin-pyproject,
 python3,
 python3-json5,
 python3-packaging,
 python3-setuptools,
 python3-setuptools-scm,
Standards-Version: 4.7.0
Homepage: https://fortls.fortran-lang.org
Vcs-Git: https://salsa.debian.org/python-team/packages/fortran-language-server.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/fortran-language-server
Rules-Requires-Root: no

Package: fortran-language-server
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends},
Recommends: ${python:Recommends}
Suggests: ${python:Suggests}, elpa-lsp-mode
Description: Fortran Language Server for the Language Server Protocol
 Fortran Language Server (fortls) is an implementation of the Language
 Server Protocol. It can be used with editors that supports the
 protocol (e.g. Emacs with elpa-lsp-mode) to offer support for code
 completion and documentation.
 .
 Supported LSP features include:
  * Document symbols (textDocument/documentSymbol)
  * Auto-complete (textDocument/completion)
  * Signature help (textDocument/signatureHelp)
  * GoTo/Peek definition (textDocument/definition)
  * Hover (textDocument/hover)
  * GoTo implementation (textDocument/implementation)
  * Find/Peek references (textDocument/references)
  * Project-wide symbol search (workspace/symbol)
  * Symbol renaming (textDocument/rename)
  * Documentation parsing (Doxygen and FORD styles)
  * Diagnostics
